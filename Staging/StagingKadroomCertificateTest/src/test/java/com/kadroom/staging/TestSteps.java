package com.kadroom.staging;

public class TestSteps extends TestBase{

    public void TestCertificatePage () {
        openCertificatePage();
        selectCertificate(all_inclusive);
        fillTheForm();
        openCertificatePage();
        selectCertificate(quest_room);
        fillTheForm();
        openCertificatePage();
        selectCertificate(cost);
        fillTheForm();
    }
    public void TestCertificatePageEn () {
        openCertificatePageEn();
        selectCertificate(all_inclusive);
        fillTheForm();
        openCertificatePageEn();
        selectCertificate(quest_room);
        fillTheForm();
        openCertificatePageEn();
        selectCertificate(cost);
        fillTheForm();
    }
    public void TestCertificatePageUa () {
        openCertificatePageUa();
        selectCertificate(all_inclusive);
        fillTheForm();
        openCertificatePageUa();
        selectCertificate(quest_room);
        fillTheForm();
        openCertificatePageUa();
        selectCertificate(cost);
        fillTheForm();
    }

    public void IncorrectNumberTest () {
        openCertificatePage();
        selectCertificate(all_inclusive);
        incorrectFillTheForm();
        incorrectNumberTrue();
    }

    public void EmptyFormTest () {
        openCertificatePage();
        selectCertificate(all_inclusive);
        emptyFillTheForm();
        emptyFormTrue();
    }
}
