package com.kadroom;

public interface TestData {

    String mainURL = "https://kadroom.com/";
    String mainURLEn = "https://kadroom.com/en/";
    String mainURLUa = "https://kadroom.com/uk/";

    String name = "test";
    String tel = "0933070545";
    String email = "ss60600@gmail.com";

    String [] invalidPhone = {"093307054","09330705455", "093307054555", "&amp;0933070545", "093" };
    String [] invalidEmail = {"NotAnEmail", "ss60600gmail.com", "ss60600@@gmail.com", "\"\"test\\blah\"\"@example.com", ".wooly@example.com", "wo..oly@example.com", "pootietang.@example.com", "Ima Fool@example.com"};
}
