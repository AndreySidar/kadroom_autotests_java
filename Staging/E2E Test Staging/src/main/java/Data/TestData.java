package Data;

public interface TestData {
    String dataBaseDriver = "com.mysql.cj.jdbc.Driver";

    String dataBaseURL = "jdbc:mysql://es93.mirohost.net:3306/kadroom_new_idudka";

    String dataBaseLogin = "u_kadroom_l";
    String dataBasePass = "18i3TkAZAbVl";

    String mainUrl = "https://staging.kadroom.com/";
    String mainUrlEn = "https://staging.kadroom.com/en/";
    String mainUrlUa = "https://staging.kadroom.com/uk/";
    String contactURL = "https://staging.kadroom.com/contactus/";
    String contactURLEn = "https://staging.kadroom.com/en/contactus/";
    String contactURLUa = "https://staging.kadroom.com/uk/contactus/";
    String corporateURL = "https://staging.kadroom.com/corporate/";
    String franchiseURL = "https://staging.kadroom.com/franchise/";
    String franchiseURLEn = "https://staging.kadroom.com/en/franchise/";
    String franchiseURLUa = "https://staging.kadroom.com/uk/franchise/";

    String multiBookingPage = "https://staging.kadroom.com/multi-booking/";
    String multiBookingPageEn = "https://staging.kadroom.com/en/multi-booking/";
    String multiBookingPageUa = "https://staging.kadroom.com/uk/multi-booking/";

    String certificateURL = "https://staging.kadroom.com/cert/";
    String certificateURLEn = "https://staging.kadroom.com/en/cert/";
    String certificateURLUa = "https://staging.kadroom.com/uk/cert/";

    String adminURL = "https://staging.kadroom.com/wp-admin/";

    String adminLog = "kadroom";
    String adminPass = "AnTBblAS^wpNjot1xceTQ$EL";

    String all_inclusive = "all_inclusive";
    String quest_room = "quest_room";
    String cost = "cost";


    String name = "test";
    String phone = "0933070545";
    String email = "ss60600@gmail.com";
    String text = "Hello from nowhere!";
    String [] invalidPhone = {"093307054","09330705455", "093307054555", "&amp;0933070545", "093" };
    String [] invalidEmail = {"NotAnEmail", "ss60600gmail.com", "ss60600@@gmail.com", "\"\"test\\blah\"\"@example.com", ".wooly@example.com", "wo..oly@example.com", "pootietang.@example.com", "Ima Fool@example.com"};

    String adminName = "Test From Admin Page";
    String adminPhone = "0933070545";
    String adminEmail = "ss60600@gmail.com";
    String adminPromo = "TestPromo";
    String textAdmin = "Hey This is RoboAdmin!";

    String cardNumber = "4242424242424242";
    String cardValidity = "1225";
    String cvv = "111";
}

