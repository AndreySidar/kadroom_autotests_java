package com.kadroom.staging;

public interface TestData {
    String certificateURL = "https://staging.kadroom.com/cert/";
    String certificateURLEn = "https://staging.kadroom.com/en/cert/";
    String certificateURLUa = "https://staging.kadroom.com/uk/cert/";

    String all_inclusive = "all_inclusive";
    String quest_room = "quest_room";
    String cost = "cost";

    String name = "test";
    String tel = "0933070545";
    String email = "ss60600@gmail.com";
    String text = "Hello from nowhere!";
}
