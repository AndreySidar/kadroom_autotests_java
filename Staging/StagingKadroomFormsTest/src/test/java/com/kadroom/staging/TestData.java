package com.kadroom.staging;

public interface TestData {
    String mainUrl = "https://staging.kadroom.com/";
    String mainUrlEn = "https://staging.kadroom.com/en/";
    String mainUrlUa = "https://staging.kadroom.com/uk/";
    String contactURL = "https://staging.kadroom.com/contactus/";
    String contactURLEn = "https://staging.kadroom.com/en/contactus/";
    String contactURLUa = "https://staging.kadroom.com/uk/contactus/";
    String corporateURL = "https://staging.kadroom.com/corporate/";
    String franchiseURL = "https://staging.kadroom.com/franchise/";
    String franchiseURLEn = "https://staging.kadroom.com/en/franchise/";
    String franchiseURLUa = "https://staging.kadroom.com/uk/franchise/";

    String name = "test";
    String tel = "0933070545";
    String email = "ss60600@gmail.com";
    String text = "Hello from nowhere!";
}
