package com.kadroom.staging;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.SocketTimeoutException;
import java.sql.*;
import java.util.List;

public class TestBase implements TestData{

    public WebDriver driver;
    public WebDriverWait wait;

    public int numberOfRoom;
    public String dateANDtime;
    public String assertDate;
    public String  assertPhone;
    public String room;
    public String assertSite;

    public int before;
    public int after;


    @Before
    public void start () {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 20);
        PageFactory.initElements(driver, this);
    }

    @After
    public void stop () {
        driver.quit();
    }

    public void openMainPage () {
        driver.navigate().to(mainURL);
    }
    public void openMainPageEn () {
        driver.navigate().to(mainURLEn);
    }
    public void openMainPageUa () {
        driver.navigate().to(mainURLUa);
    }

    public void selectRoom () {
        List<WebElement> rooms = driver.findElements(By.xpath("//div[@class='rooms-w']/a[@itemscope][*]"));

        numberOfRoom = (int) (Math.random()*rooms.size());
        if (numberOfRoom > 0){
            WebElement selectRoom = driver.findElement(By.xpath("//div[@class='rooms-w']/a[@itemscope][" + numberOfRoom + "]"));
            selectRoom.click();
        }else {
            selectRoom();
        }
    }



    public void selectGameTime(){
        List <WebElement> dates = driver.findElements(By.xpath("//div[@data-datetime]"));
        int j = (int) (Math.random()*dates.size());
        dateANDtime = dates.get(j).getAttribute("data-datetime");
        try {
            do {
                try {
                    WebElement dynamicElement = (new WebDriverWait(driver, 1))
                            .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-datetime='"+ dateANDtime + "']")));
                    if (dynamicElement.isDisplayed()){
                        dynamicElement.click();
                        break;
                    }
                }catch (Exception e){
                    driver.findElement(By.xpath("//*[@id=\"calendar\"]/div[2]/button[2]")).click();
                }
            }while (true);
        }catch (Exception ignored){}
    }


    public void fillTheForm () {
        wait.until(ExpectedConditions.visibilityOfAllElements(bookDate,roomName,phone,promo,buttonRegister));
        room = roomName.getText();
        phone.clear();
        phone.click();
        phone.sendKeys(tel);
        promo.clear();
        promo.click();
        promo.sendKeys(name);
        buttonRegister.click();
        wait.until(ExpectedConditions.visibilityOf(thankYou));
        System.out.println("Забронировано: " + room +" ---- " + dateANDtime);
    }

    public void emptyFillForm () {
        openMainPage();
        selectRoom();
        selectGameTime();
        wait.until(ExpectedConditions.visibilityOfAllElements(bookDate,roomName,phone,promo,buttonRegister));
        buttonRegister.click();
        if (error.isDisplayed()){

        }else {
            System.out.println("Валидация на пустое поле не работает!");
        }
    }

    public void invalidFillForm () {
        for (String value : invalidPhone) {
            openMainPage();
            selectRoom();
            selectGameTime();
            wait.until(ExpectedConditions.visibilityOfAllElements(bookDate, roomName, phone, promo, buttonRegister));
            phone.sendKeys(value);
            buttonRegister.click();
            if (error.isDisplayed()) {

            } else {
                wait.until(ExpectedConditions.visibilityOf(thankYou));
                System.out.println("Валидация не работает!" + value);
                try {
                    Class.forName(dataBaseDriver);
                    Connection conn = DriverManager.getConnection(dataBaseURL,
                            dataBaseLogin, dataBasePass);
                    Statement stmt=conn.createStatement();
                    int rs=stmt.executeUpdate("UPDATE `reservation` SET `status` = 'canceled' WHERE `reservation`.`phone` = '"+ value + "'");
                    conn.close();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    public void connectingToDatabase () {
            try {
                Class.forName(dataBaseDriver);
                Connection conn = DriverManager.getConnection(dataBaseURL,
                        dataBaseLogin, dataBasePass);
                Statement stmt=conn.createStatement();
                ResultSet rs=stmt.executeQuery("SELECT * FROM reservation");

                while(rs.next()){
                    before = rs.getInt(1);
                }
                conn.close();
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    public void connectingToDatabaseResult () {
        try {
            Class.forName(dataBaseDriver);
            Connection conn = DriverManager.getConnection(dataBaseURL,
                    dataBaseLogin, dataBasePass);
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT * FROM reservation");

            while(rs.next()){
                after = rs.getInt(1);
                assertDate = rs.getString(4);
                assertPhone = rs.getString("phone");
                assertSite = rs.getString("site");
            }
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void cancelReservation () {
        try {
            Class.forName(dataBaseDriver);
            Connection conn = DriverManager.getConnection(dataBaseURL,
                    dataBaseLogin, dataBasePass);
            Statement stmt=conn.createStatement();
            boolean rs=stmt.execute("UPDATE `reservation` SET `status` = 'canceled' WHERE `reservation`.`id` =" + after);
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void assertion () {
        Assert.assertNotEquals(before, after);
        Assert.assertEquals(dateANDtime, assertDate);
        Assert.assertEquals(tel, assertPhone);
        Assert.assertEquals("kadroom", assertSite);
    }

    @FindBy(name = "phone")
    public WebElement phone;

    @FindBy(name = "promo")
    public WebElement promo;

    @FindBy(xpath = "//div[@class='register-button']")
    public WebElement buttonRegister;

    @FindBy(xpath = "//div[@class='booking-name js_name']")
    public WebElement roomName;

    @FindBy(xpath = "//span[@class='js_select_date_text']")
    public WebElement bookDate;

    @FindBy(xpath = "//h1[@class='section-title']")
    public WebElement thankYou;

    @FindBy(xpath = "//div[@id='result']")
    public WebElement error;
}
