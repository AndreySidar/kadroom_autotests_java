package TestData;

public interface TestData {

    String mainUrl = "https://staging.kadroom.com/";
    String mainUrlEn = "https://staging.kadroom.com/en/";
    String mainUrlUa = "https://staging.kadroom.com/uk/";

    String contactURL = "https://staging.kadroom.com/contactus/";
    String contactURLEn = "https://staging.kadroom.com/en/contactus/";
    String contactURLUa = "https://staging.kadroom.com/uk/contactus/";

    String corporateURL = "https://staging.kadroom.com/corporate/";

    String franchiseURL = "https://staging.kadroom.com/franchise/";
    String franchiseURLEn = "https://staging.kadroom.com/en/franchise/";
    String franchiseURLUa = "https://staging.kadroom.com/uk/franchise/";

    String faqURL = "https://staging.kadroom.com/faq/";
    String faqURLEn = "https://staging.kadroom.com/en/faq/";
    String faqURLUa = "https://staging.kadroom.com/uk/faq/";

    String certificateURL = "https://staging.kadroom.com/cert/";
    String certificateURLEn = "https://staging.kadroom.com/en/cert/";
    String certificateURLUa = "https://staging.kadroom.com/uk/cert/";

    String all_inclusive = "all_inclusive";
    String quest_room = "quest_room";
    String cost = "cost";

    String name = "test";
    String phone = "0933070545";
    String email = "ss60600@gmail.com";
    String text = "Hello from nowhere!";


    String dataBaseDriver = "com.mysql.cj.jdbc.Driver";

    String dataBaseURL = "jdbc:mysql://es93.mirohost.net:3306/kadroom_new_idudka";

    String dataBaseLogin = "u_kadroom_l";
    String dataBasePass = "18i3TkAZAbVl";
}
