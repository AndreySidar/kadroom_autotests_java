package TestData;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Locators {

    @FindBy(xpath = "//span[@class='writeus-title']")
    public WebElement writeUs;

    @FindBy(name = "text-104")
    public WebElement userName;

    @FindBy(name = "tel-193")
    public WebElement userPhone;

    @FindBy(name = "email-795")
    public WebElement userEmail;

    @FindBy(name = "textarea-726")
    public WebElement userText;

    @FindBy(xpath = "//input[@type='submit']")
    public WebElement submitButtonForm;

    @FindBy(xpath = "//button")
    public WebElement button;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement incorrectNumber;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement emptyForm;

    @FindBy(xpath = "//*[@id='cf7-rooms']")
    public WebElement rooms;

    @FindBy(xpath = "//*[@id='cf7-prices']")
    public WebElement prices;

    @FindBy(xpath = "//*[@id='cf7-payment']")
    public WebElement payment;

    @FindBy(name = "tel-193")
    public WebElement userTel;

    @FindBy(className = "cf7-body")
    public WebElement body;


    @FindBy(name = "phone")
    public WebElement userPhoneBook;

    @FindBy(name = "promo")
    public WebElement userPromo;

    @FindBy(xpath = "//*[@id='register']")
    public WebElement buttonRegister;

    @FindBy(xpath = "//div[@class='booking-name js_name']")
    public WebElement roomName;

    @FindBy(xpath = "//span[@class='js_select_date_text']")
    public WebElement bookDate;

    @FindBy(xpath = "//div[@class ='thanks-links']")
    public WebElement thankYou;

    @FindBy(xpath = "//div[@id='result']")
    public WebElement error;

    @FindBy(className = "wpcf7-mail-sent-ok")
    public WebElement alertOK;

    @FindBy(className = "wpcf7-validation-errors")
    public WebElement alertError;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement alert;

    @FindBy(className = "ajax-loader")
    public WebElement loader;

    @FindBy(name = "payment_method")
    public WebElement paymentMethod;

    @FindBy(className = "input card")
    public WebElement inputCard;

    @FindBy(name = "cardExpirationMonth")
    public WebElement cardExpiration;

    @FindBy(name = "cvv2")
    public WebElement cvv2;

    @FindBy(className = "button__send")
    public WebElement buttonSend;

    @FindBy(className = "container__block__confirm__success__text")
    public WebElement paymentOk;
}
