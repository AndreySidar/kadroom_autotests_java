package Entites;

import TestData.Locators;
import TestData.TestData;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class FormTest extends Locators implements TestData {

    private WebDriver driver;

    public FormTest (WebDriver driver){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
        this.driver = driver;
    }

    public void clickWriteUsLable () {

        Actions actions = new Actions(driver);

        actions.click(writeUs).build().perform();
    }

    public void clickButton () {

        Actions actions = new Actions(driver);

        actions.click(button).build().perform();
    }

    public void fillTheForm () {

        Actions actions = new Actions(driver);

        actions.sendKeys(userName, name);
        actions.sendKeys(userPhone, phone);
        actions.sendKeys(userEmail, email);
        actions.sendKeys(userText, text);

        actions.click(submitButtonForm).build().perform();
    }

    public void fillThePresaleForm () {
        Actions actions = new Actions(driver);

        actions.sendKeys(userName, name);
        actions.sendKeys(userEmail, email);
        actions.sendKeys(userTel, text);

        actions.click(submitButtonForm).build().perform();
    }

    public void incorrectFillTheForm () {

        Actions actions = new Actions(driver);

        actions.sendKeys(userName, name);
        actions.sendKeys(userPhone, name);
        actions.sendKeys(userEmail, email);
        actions.sendKeys(userText, text);

        actions.click(submitButtonForm).build().perform();
    }

    public void emptyFillTheForm () {
        submitButtonForm.click();
    }

    public void incorrectNumberTrue () {
        Assert.assertTrue("Некорректный номер телефона.", incorrectNumber.isDisplayed());
    }

    public void emptyFormTrue () {
        Assert.assertTrue("Пожалуйста, укажите телефон или email!", incorrectNumber.isDisplayed());
    }

}
