package Pages;

import Data.TestData;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class CertificatePage implements TestData {

    private WebDriver driver;

    public String typeOfCertificate;

    public String nameOfCertificate;

    @FindBy(name = "text-104")
    public WebElement userName;

    @FindBy(name = "tel-193")
    public WebElement userPhone;

    @FindBy(name = "email-795")
    public WebElement userEmail;

    @FindBy(name = "textarea-726")
    public WebElement userText;

    @FindBy(xpath = "//input[@type='submit']")
    public WebElement submitButtonForm;

    @FindBy(id = "cf7-rooms")
    public WebElement rooms;

    @FindBy(id = "cf7-prices")
    public WebElement prices;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement incorrectNumber;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement emptyForm;

    @FindBy(name = "payment")
    public WebElement payment;


    public CertificatePage (WebDriver driver){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
        this.driver = driver;
    }

    public void open () {
        driver.navigate().to(certificateURL);
    }
    public void openEn () {
        driver.navigate().to(certificateURLEn);
    }
    public void openUa () {
        driver.navigate().to(certificateURLUa);
    }

    public void selectCertificate (String type) {
        List<WebElement> certificateType = driver.findElements(By.xpath("//div[@class='cert__item-box contact-form js-give-certificate']"));
        for (WebElement webElement : certificateType) {
            typeOfCertificate = webElement.getAttribute("data-type");
            nameOfCertificate = webElement.getAttribute("data-name");
            if (typeOfCertificate.equals(type)){

                switch (typeOfCertificate) {
                    case all_inclusive:
                        webElement.click();
                        System.out.println("Забронировано: Сертификат " + nameOfCertificate);
                        break;
                    case quest_room: {
                        webElement.click();
                        Select select = new Select(rooms);
                        List options = select.getOptions();
                        int index = (int) (Math.random() * options.size());
                        select.selectByIndex(index);
                        do {
                            index = (int) (Math.random() * options.size());
                            select.selectByIndex(index);
                        } while (index == 1);
                        System.out.println("Забронировано: Сертификат " + nameOfCertificate + " " + rooms.getAttribute("value"));
                        break;
                    }
                    case cost: {
                        webElement.click();
                        Select select = new Select(prices);
                        List options = select.getOptions();
                        int index = (int) (Math.random() * options.size());
                        select.selectByIndex(index);
                        do {
                            index = (int) (Math.random() * options.size());
                            select.selectByIndex(index);
                        } while (index == 1);
                        System.out.println("Забронировано: Сертификат " + nameOfCertificate + " " + prices.getAttribute("value"));
                        break;
                    }
                }
            }
        }
    }

    public void fillTheForm () {
        userName.sendKeys(name);
        userPhone.sendKeys(phone);
        userEmail.sendKeys(email);
        userText.sendKeys(text);
        submitButtonForm.click();
    }
    public void incorrectFillTheForm () {
        userName.sendKeys(name);
        userPhone.sendKeys(name);
        userEmail.sendKeys(email);
        userText.sendKeys(text);
        submitButtonForm.click();
    }
    public void emptyFillTheForm () {
        submitButtonForm.click();
    }

    public void incorrectNumberTrue () {
        assertEquals("Некорректный номер телефона.", incorrectNumber.getText());
    }
    public void emptyFormTrue () {
        Assert.assertEquals("Пожалуйста, укажите телефон или email!", emptyForm.getText());
    }

    public void selectCertificateOnlinePayment (String type) {
        List<WebElement> certificateType = driver.findElements(By.xpath("//div[@class='cert__item-box contact-form js-give-certificate']"));
        for (WebElement webElement : certificateType) {
            typeOfCertificate = webElement.getAttribute("data-type");
            nameOfCertificate = webElement.getAttribute("data-name");
            if (typeOfCertificate.equals(type)){

                switch (typeOfCertificate) {
                    case all_inclusive: {
                        webElement.click();
                        Select select = new Select(payment);
                        select.selectByIndex(1);
                        System.out.println("Забронировано онлайн: Сертификат " + nameOfCertificate);
                        break;
                    }
                    case quest_room: {
                        webElement.click();
                        Select select = new Select(rooms);
                        List options = select.getOptions();
                        int index = (int) (Math.random() * options.size());
                        select.selectByIndex(index);
                        do {
                            index = (int) (Math.random() * options.size());
                            select.selectByIndex(index);
                        } while (index == 1);
                        select = new Select(payment);
                        select.selectByIndex(1);
                        System.out.println("Забронировано онлайн: Сертификат " + nameOfCertificate + " " + rooms.getAttribute("value"));
                        break;
                    }
                    case cost: {
                        webElement.click();
                        Select select = new Select(prices);
                        List options = select.getOptions();
                        int index = (int) (Math.random() * options.size());
                        select.selectByIndex(index);
                        do {
                            index = (int) (Math.random() * options.size());
                            select.selectByIndex(index);
                        } while (index == 1);
                        select = new Select(payment);
                        select.selectByIndex(1);
                        System.out.println("Забронировано онлайн: Сертификат " + nameOfCertificate + " " + prices.getAttribute("value"));
                        break;
                    }
                }
            }
        }
    }

    public void waitForOnlinePayment () {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("LiqPay Dudka Agency"));
        String title = driver.getTitle();
        assertTrue(title.contains("LiqPay Dudka Agency"));
    }
}
