package com.kadroom;

public interface TestData {
    String mainUrl = "https://kadroom.com/";
    String mainUrlEn = "https://kadroom.com/en/";
    String mainUrlUa = "https://kadroom.com/uk/";
    String contactURL = "https://kadroom.com/contactus/";
    String contactURLEn = "https://kadroom.com/en/contactus/";
    String contactURLUa = "https://kadroom.com/uk/contactus/";
    String corporateURL = "https://kadroom.com/corporate/";
    String franchiseURL = "https://kadroom.com/franchise/";
    String franchiseURLEn = "https://kadroom.com/en/franchise/";
    String franchiseURLUa = "https://kadroom.com/uk/franchise/";

    String name = "test";
    String tel = "0933070545";
    String email = "ss60600@gmail.com";
    String text = "Hello from Dudka Agency!\n This is Test...\n We are sorry)";
}
