package Pages;

import Entites.FormTest;
import TestData.Locators;
import TestData.TestData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import java.util.List;

public class MainPage extends Locators implements TestData {

    private WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
        this.driver = driver;
    }


    /*---------ОТКРЫТИЕ ГЛАВНОЙ СТРАНИЦИ---------*/

    public void openMainPage() {
        driver.navigate().to(mainUrl);
    }

    public void openMainPageEn() {
        driver.navigate().to(mainUrlEn);
    }

    public void openMainPageUa() {
        driver.navigate().to(mainUrlUa);
    }

    public void selectPresale() {

        List<WebElement> rooms = driver.findElements(By.className("room-announce-btn"));
        int random = (int) (Math.random() * rooms.size());
        rooms.get(random).click();

        FormTest formTest = new FormTest(driver);

        formTest.fillThePresaleForm();
    }

    public void fillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.clickWriteUsLable();
        formTest.fillTheForm();
    }

    public void incorrectFillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.clickWriteUsLable();
        formTest.incorrectFillTheForm();
        formTest.incorrectNumberTrue();
    }

    public void emptyFillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.clickWriteUsLable();
        formTest.emptyFillTheForm();
        formTest.emptyFormTrue();
    }
}
