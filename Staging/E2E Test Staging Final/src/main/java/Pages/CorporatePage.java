package Pages;

import Entites.FormTest;
import TestData.TestData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class CorporatePage implements TestData {

    private WebDriver driver;

    public CorporatePage(WebDriver driver) {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
        this.driver = driver;
    }

    public void openCorporatePage() {
        driver.navigate().to(corporateURL);
    }

    public void fillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.clickButton();
        formTest.fillTheForm();

    }

    public void incorrectFillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.clickButton();
        formTest.incorrectFillTheForm();
        formTest.incorrectNumberTrue();
    }

    public void emptyFillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.clickButton();
        formTest.emptyFillTheForm();
        formTest.emptyFormTrue();
    }
}
