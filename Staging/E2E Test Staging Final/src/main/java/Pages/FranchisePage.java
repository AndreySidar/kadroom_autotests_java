package Pages;

import Entites.FormTest;
import TestData.TestData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class FranchisePage implements TestData {

    private WebDriver driver;

    public FranchisePage(WebDriver driver) {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
        this.driver = driver;
    }

    public void openFranchisePage() {
        driver.navigate().to(franchiseURL);
    }

    public void openFranchisePageEn() {
        driver.navigate().to(franchiseURLEn);
    }

    public void openFranchisePageUa() {
        driver.navigate().to(franchiseURLUa);
    }

    public void fillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.clickButton();
        formTest.fillTheForm();

    }

    public void incorrectFillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.clickButton();
        formTest.incorrectFillTheForm();
        formTest.incorrectNumberTrue();
    }

    public void emptyFillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.clickButton();
        formTest.emptyFillTheForm();
        formTest.emptyFormTrue();
    }
}
