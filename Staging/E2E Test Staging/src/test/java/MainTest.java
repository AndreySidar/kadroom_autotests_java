import Data.TestData;
import Pages.BookingPage;
import Pages.CertificatePage;
import Pages.EscapeRoomPage;
import Tests.AdminBookingTest;
import Tests.FormsTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class MainTest {

    private WebDriver driver;

    /*---------ИНИЦИАЛИЗАЦИЯ ДРАЙВЕРА---------
    *        Происходит до вызова класса       */

    @Before
    public void start () {
        System.setProperty("webdriver.chrome.driver", "chromedriver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        PageFactory.initElements(driver, this);
    }

    //---------ПОСЛЕ ВЫПОЛНЕНИЯ ТЕСТОВ ВЫХОД ИЗ ДРАЙВЕРА---------//

    @After
    public void stop () {
        driver.quit();
    }


    //----------НАЧИНАЮТСЯ ТЕСТЫ -------------//

    @Test
    public void escapeRoomPageTest () {
        EscapeRoomPage escapeRoomPage = new EscapeRoomPage(driver);

        /*----------ТЕСТИРОВАНИЕ ОДИНОЧНОГО БРОНИРОВАНИЯ---------*/

        //ТЕСТИРОВАНИЕ ОДИНОЧНОГО БРОНИРОВАНИЯ РУС ВЕРСИЯ

        System.out.println("----ТЕСТИРОВАНИЕ ОДИНОЧНОГО БРОНИРОВАНИЯ РУС ВЕРСИЯ----");

        escapeRoomPage.open();
        escapeRoomPage.connectingToDatabase();
        escapeRoomPage.selectRoom();
        escapeRoomPage.selectGameTime();
        escapeRoomPage.fillTheForm();
        escapeRoomPage.connectingToDatabaseResult();
        escapeRoomPage.checkSms();
        escapeRoomPage.cancelReservation();
        escapeRoomPage.assertion();

        //ТЕСТИРОВАНИЕ ОДИНОЧНОГО БРОНИРОВАНИЯ АНГЛ ВЕРСИЯ

        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ОДИНОЧНОГО БРОНИРОВАНИЯ АНГЛ ВЕРСИЯ----");

        escapeRoomPage.openEn();
        escapeRoomPage.connectingToDatabase();
        escapeRoomPage.selectRoom();
        escapeRoomPage.selectGameTime();
        escapeRoomPage.fillTheForm();
        escapeRoomPage.connectingToDatabaseResult();
        escapeRoomPage.checkSms();
        escapeRoomPage.cancelReservation();
        escapeRoomPage.assertion();

        //ТЕСТИРОВАНИЕ ОДИНОЧНОГО БРОНИРОВАНИЯ УКР ВЕРСИЯ

        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ОДИНОЧНОГО БРОНИРОВАНИЯ УКР ВЕРСИЯ----");

        escapeRoomPage.openUa();
        escapeRoomPage.connectingToDatabase();
        escapeRoomPage.selectRoom();
        escapeRoomPage.selectGameTime();
        escapeRoomPage.fillTheForm();
        escapeRoomPage.connectingToDatabaseResult();
        escapeRoomPage.checkSms();
        escapeRoomPage.cancelReservation();
        escapeRoomPage.assertion();

        //ТЕСТИРОВАНИЕ БРОНИРОВАНИЯ С ОНЛАЙН ОПЛАТОЙ

        System.out.println("\n");

        escapeRoomPage.open();
        escapeRoomPage.selectRoom();
        escapeRoomPage.selectGameTime();
        escapeRoomPage.fillTheFormOnlinePay();
        escapeRoomPage.waitForOnlinePayment();
        escapeRoomPage.connectingToDatabaseResult();
        escapeRoomPage.assertionOnlinePay();

        escapeRoomPage.cancelReservation();


        escapeRoomPage.cancelAllReservation();

        /*----------ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ---------*/

        //ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ РУС ВЕРСИЯ

        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ РУС ВЕРСИЯ----");

        escapeRoomPage.open();
        escapeRoomPage.selectPresale();

        //ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ АНГЛ ВЕРСИЯ

        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ АНГЛ ВЕРСИЯ----");

        escapeRoomPage.openEn();
        escapeRoomPage.selectPresale();

        //ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ УКР ВЕРСИЯ

        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ УКР ВЕРСИЯ----");

        escapeRoomPage.openUa();
        escapeRoomPage.selectPresale();

    }

    @Test
    public void bookingPageTest () {
        BookingPage bookingPage = new BookingPage(driver);

        /*---------ТЕСТИРОВАНИЕ МУЛЬТИБУКИНГА---------*/

        //ТЕСТИРОВАНИЕ МУЛЬТИБУКИНГА РУС ВЕРСИЯ

        System.out.println("----ТЕСТИРОВАНИЕ МУЛЬТИБУКИНГА РУС ВЕРСИЯ----");

        bookingPage.open();
        bookingPage.selectRooms();
        bookingPage.fillTheForm();
        bookingPage.connectingToDatabase();

        //ТЕСТИРОВАНИЕ МУЛЬТИБУКИНГА АНГЛ ВЕРСИЯ

        //ТЕСТИРОВАНИЕ МУЛЬТИБУКИНГА УКР ВЕРСИЯ
    }

    @Test
    public void certificatePageTest () {
        CertificatePage certificatePage = new CertificatePage(driver);

        /*----------ТЕСТИРОВАНИЕ СЕРТИФИКАТОВ---------*/

        //ТЕСТИРОВАНИЕ СЕРТИФИКАТОВ РУС ВЕРСИЯ

        System.out.println("----БРОНИРОВАНИЕ СЕРТИФИКАТА НА РУС ЯЗЫКЕ----");

        certificatePage.open();
        certificatePage.selectCertificate(TestData.all_inclusive);
        certificatePage.fillTheForm();

        certificatePage.open();
        certificatePage.selectCertificate(TestData.quest_room);
        certificatePage.fillTheForm();

        certificatePage.open();
        certificatePage.selectCertificate(TestData.cost);
        certificatePage.fillTheForm();

        System.out.println("\n");
        System.out.println("----БРОНИРОВАНИЕ СЕРТИФИКАТА ОНЛАЙН НА РУС ЯЗЫКЕ----");

        certificatePage.open();
        certificatePage.selectCertificateOnlinePayment(TestData.all_inclusive);
        certificatePage.fillTheForm();
        certificatePage.waitForOnlinePayment();

        certificatePage.open();
        certificatePage.selectCertificateOnlinePayment(TestData.quest_room);
        certificatePage.fillTheForm();
        certificatePage.waitForOnlinePayment();

        certificatePage.open();
        certificatePage.selectCertificateOnlinePayment(TestData.cost);
        certificatePage.fillTheForm();
        certificatePage.waitForOnlinePayment();

        //ТЕСТИРОВАНИЕ СЕРТИФИКАТОВ АНГЛ ВЕРСИЯ

        System.out.println("\n");
        System.out.println("----БРОНИРОВАНИЕ СЕРТИФИКАТА НА АНГЛ ЯЗЫКЕ----");

        certificatePage.openEn();
        certificatePage.selectCertificate(TestData.all_inclusive);
        certificatePage.fillTheForm();

        certificatePage.openEn();
        certificatePage.selectCertificate(TestData.quest_room);
        certificatePage.fillTheForm();

        certificatePage.openEn();
        certificatePage.selectCertificate(TestData.cost);
        certificatePage.fillTheForm();

        System.out.println("\n");
        System.out.println("----БРОНИРОВАНИЕ СЕРТИФИКАТА ОНЛАЙН НА АНГЛ ЯЗЫКЕ----");

        certificatePage.openEn();
        certificatePage.selectCertificateOnlinePayment(TestData.all_inclusive);
        certificatePage.fillTheForm();
        certificatePage.waitForOnlinePayment();

        certificatePage.openEn();
        certificatePage.selectCertificateOnlinePayment(TestData.quest_room);
        certificatePage.fillTheForm();
        certificatePage.waitForOnlinePayment();

        certificatePage.openEn();
        certificatePage.selectCertificateOnlinePayment(TestData.cost);
        certificatePage.fillTheForm();
        certificatePage.waitForOnlinePayment();

        //ТЕСТИРОВАНИЕ СЕРТИФИКАТОВ УКР ВЕРСИЯ

        System.out.println("\n");
        System.out.println("----БРОНИРОВАНИЕ СЕРТИФИКАТА НА УКР ЯЗЫКЕ----");

        certificatePage.openUa();
        certificatePage.selectCertificate(TestData.all_inclusive);
        certificatePage.fillTheForm();

        certificatePage.openUa();
        certificatePage.selectCertificate(TestData.quest_room);
        certificatePage.fillTheForm();

        certificatePage.openUa();
        certificatePage.selectCertificate(TestData.cost);
        certificatePage.fillTheForm();

        System.out.println("\n");
        System.out.println("----БРОНИРОВАНИЕ СЕРТИФИКАТА ОНЛАЙН НА УКР ЯЗЫКЕ----");

        certificatePage.openUa();
        certificatePage.selectCertificateOnlinePayment(TestData.all_inclusive);
        certificatePage.fillTheForm();
        certificatePage.waitForOnlinePayment();

        certificatePage.openUa();
        certificatePage.selectCertificateOnlinePayment(TestData.quest_room);
        certificatePage.fillTheForm();
        certificatePage.waitForOnlinePayment();

        certificatePage.openUa();
        certificatePage.selectCertificateOnlinePayment(TestData.cost);
        certificatePage.fillTheForm();
        certificatePage.waitForOnlinePayment();

        //НЕГАТИВНОЕ ТЕСТИРОВАНИЕ СЕРТИФИКАТОВ

        System.out.println("\n");
        System.out.println("----НЕГАТИВНОЕ ТЕСТИРОВАНИЕ БРОНИРОВАНИЕ СЕРТИФИКАТА----");

        certificatePage.open();
        certificatePage.selectCertificate(TestData.all_inclusive);
        certificatePage.emptyFillTheForm();
        certificatePage.emptyFormTrue();

        certificatePage.open();
        certificatePage.selectCertificate(TestData.all_inclusive);
        certificatePage.incorrectFillTheForm();
        certificatePage.incorrectNumberTrue();
    }

    @Test
    public void formsTest () {
        FormsTest formsTest = new FormsTest(driver);

        /*----------ТЕСТИРОВАНИЕ ФОРМ ОБРАТНОЙ СВЯЗИ----------*/

        //ТЕСТИРОАНИЕ ФОРМ ГЛАВНАЯ СТРАНИЦА

        System.out.println("----ТЕСТИРОАНИЕ ФОРМ ГЛАВНАЯ СТРАНИЦА----");

        formsTest.openMainPage();
        formsTest.writeUsLableClick();
        formsTest.fillTheForm();
        formsTest.openMainPageEn();
        formsTest.writeUsLableClick();
        formsTest.fillTheForm();
        formsTest.openMainPageUa();
        formsTest.writeUsLableClick();
        formsTest.fillTheForm();

        //ТЕСТИРОАНИЕ ФОРМ СТРАНИЦА КОНТАКТЫ

        System.out.println("\n");
        System.out.println("----ТЕСТИРОАНИЕ ФОРМ СТРАНИЦА КОНТАКТЫ----");

        formsTest.openContactPage();
        formsTest.fillTheForm();
        formsTest.openContactPageEn();
        formsTest.fillTheForm();
        formsTest.openContactPageUa();
        formsTest.fillTheForm();

        //ТЕСТИРОАНИЕ ФОРМ КОРПОРАТИВНАЯ СТРАНИЦА

        System.out.println("\n");
        System.out.println("----ТЕСТИРОАНИЕ ФОРМ КОРПОРАТИВНАЯ СТРАНИЦА----");

        formsTest.openCorporatePage();
        formsTest.buttonClick();
        formsTest.fillTheForm();

        //ТЕСТИРОАНИЕ ФОРМ СТРАНИЦА ФРАНЧИЗИ

        System.out.println("\n");
        System.out.println("----ТЕСТИРОАНИЕ ФОРМ СТРАНИЦА ФРАНЧИЗИ----");

        formsTest.openFranchisePage();
        formsTest.buttonClick();
        formsTest.fillTheForm();
        formsTest.openFranchisePageEn();
        formsTest.buttonClick();
        formsTest.fillTheForm();
        formsTest.openFranchisePageUa();
        formsTest.buttonClick();
        formsTest.fillTheForm();

        //НЕГАТИВНОЕ ТЕСТИРОВАНИЕ ФОРМ

        System.out.println("\n");
        System.out.println("----НЕГАТИВНОЕ ТЕСТИРОВАНИЕ ФОРМ----");

        formsTest.openMainPage();
        formsTest.writeUsLableClick();
        formsTest.incorrectFillTheForm();
        formsTest.incorrectNumberTrue();

        formsTest.openContactPage();
        formsTest.incorrectFillTheForm();
        formsTest.incorrectNumberTrue();

        formsTest.openFranchisePage();
        formsTest.buttonClick();
        formsTest.incorrectFillTheForm();
        formsTest.incorrectNumberTrue();

        formsTest.openCorporatePage();
        formsTest.buttonClick();
        formsTest.incorrectFillTheForm();
        formsTest.incorrectNumberTrue();


        formsTest.openMainPage();
        formsTest.writeUsLableClick();
        formsTest.emptyFillTheForm();
        formsTest.emptyFormTrue();

        formsTest.openContactPage();
        formsTest.emptyFillTheForm();
        formsTest.emptyFormTrue();

        formsTest.openFranchisePage();
        formsTest.buttonClick();
        formsTest.emptyFillTheForm();
        formsTest.emptyFormTrue();

        formsTest.openCorporatePage();
        formsTest.buttonClick();
        formsTest.emptyFillTheForm();
        formsTest.emptyFormTrue();
    }

    @Test
    public void adminBookingTest () {
        AdminBookingTest adminBookingTest = new AdminBookingTest(driver);

        /*---------ТЕСТИРОВАНИЕ БРОНИРОВАНИЯ ИЗ ПОД АДМИНА---------*/

        System.out.println("----ТЕСТИРОВАНИЕ БРОНИРОВАНИЯ ИЗ ПОД АДМИНА----");

        adminBookingTest.openAdminPage();
        adminBookingTest.openModerateRooms();
        adminBookingTest.selectRoom();
        adminBookingTest.fillForm();
        adminBookingTest.cancelFromDatabase();
        adminBookingTest.assertion();

    }
}
