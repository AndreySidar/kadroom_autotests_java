package Pages;

import Data.TestData;
import junit.framework.AssertionFailedError;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNotEquals;

public class EscapeRoomPage implements TestData {

    //----------ОБЯВЛЕНИЕ НУЖНЫХ ПЕРЕМЕНЫХ----------//

    private WebDriver driver;
    public WebDriverWait wait;

    public int numberOfRoom;
    public String dateANDtime;
    public String assertDate;
    public String  assertPhone;
    public String room;
    public String assertSite;

    public int before;
    public int after;

    public int databaseSmsID;

    public int smsID;
    public String smsText;
    public String smsPhone;
    public String smsStatus;

    public String secretKey;

    public String payment;

    public String referer;

    //----------ПОИСК ЭЛЕМЕНТОВ НА СТРАНИЦЕ----------//

    @FindBy(name = "phone")
    public WebElement userPhone;

    @FindBy(name = "promo")
    public WebElement userPromo;

    @FindBy(id = "register")
    public WebElement buttonRegister;

    @FindBy(xpath = "//div[@class='booking-name js_name']")
    public WebElement roomName;

    @FindBy(xpath = "//span[@class='js_select_date_text']")
    public WebElement bookDate;

    @FindBy(xpath = "//h1[@class='section-title']")
    public WebElement thankYou;

    @FindBy(xpath = "//div[@id='result']")
    public WebElement error;

    @FindBy(name = "text-104")
    public WebElement userName;

    @FindBy(name = "tel-193")
    public WebElement userTel;

    @FindBy(name = "email-795")
    public WebElement userEmail;

    @FindBy(xpath = "//input[@type='submit']")
    public WebElement submitButtonForm;

    @FindBy(className = "wpcf7-mail-sent-ok")
    public WebElement alertOK;

    @FindBy(className = "wpcf7-validation-errors")
    public WebElement alertError;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement alert;

    @FindBy(className = "ajax-loader")
    public WebElement loader;

    @FindBy(name = "payment_method")
    public WebElement paymentMethod;

    @FindBy(className = "input card")
    public WebElement inputCard;

    @FindBy(name = "cardExpirationMonth")
    public WebElement cardExpiration;

    @FindBy(name = "cvv2")
    public WebElement cvv2;

    @FindBy(className = "button__send")
    public WebElement buttonSend;

    @FindBy(className = "container__block__confirm__success__text")
    public WebElement paymentOk;

    //----------ОБЯВЛЕНИЕ КЛАСА И ИНИЦИАЛИЗАЦИЯ PAGE OBJECT ПАТТЕРНА----------//

    public EscapeRoomPage (WebDriver driver){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
        this.driver = driver;
    }

    //----------ОТКРЫТИЕ СТРАНИЦ----------//

    public void open () {
        driver.navigate().to(mainUrl);}                                         //Открытие главной страници на рус языке
    public void openEn () {
        driver.navigate().to(mainUrlEn);}                                       //Открытие главной страници на англ языке
    public void openUa () {
        driver.navigate().to(mainUrlUa); }                                      //Открытие главной страници на укр языке

    /*----------ТЕСТИРОВАНИЕ ОДИНОЧНОГО БРОНИРОВАНИЯ----------*/

    public void selectRoom() {

        try {                                                                   //Подключение к базе данны для выборки нужной информации
            Class.forName(dataBaseDriver);                                      // в данно случае берем ID sms до бронирования комнаты
            Connection conn = DriverManager.getConnection(dataBaseURL,          // что бы в дальнейшем проверить что новое сообщение создалось
                    dataBaseLogin, dataBasePass);                               // и ему присвоилось новое ID отличное от этого
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT * FROM logs_sms");       //Выборка данных из БД

            while(rs.next()){
                databaseSmsID = rs.getInt("id");                    //Выбираем значение из колонки с названием id
            }
            conn.close();                                                       //Закрываем подключение
        }catch(Exception e){
            e.printStackTrace();
        }


        List<WebElement> rooms = driver.findElements(By.xpath("//div[@class='rooms-w']/a[@itemscope][*]"));                             //Создаем Массив из комнат кроме пресейлов
        numberOfRoom = (int) (Math.random()*rooms.size());                                                                              //Рандомно выбираем одну комнату
        if (numberOfRoom > 0){                                                                                                          //Если номер комнаты больше 0 то выполняем следующий код
            WebElement selectRoom = driver.findElement(By.xpath("//div[@class='rooms-w']/a[@itemscope][" + numberOfRoom + "]"));        //Выбираем комнату на локатору под номером полученным ранее
            selectRoom.click();                                                                                                         //Клик на комнату
        }else {
            selectRoom();                                                                                                               //Если комнат не оказалось повторяем все с начала
        }
    }

    public void selectGameTime(){                                                                                                       //Выбираем игровое время из доступных слотов
        List <WebElement> dates = driver.findElements(By.xpath("//div[@data-datetime]"));                                               //Создаем Массив из доступных слотов
        int j = (int) (Math.random()*dates.size());                                                                                     //Рандомно выбираем слот
        dateANDtime = dates.get(j).getAttribute("data-datetime");                                                                    //Сохраняем время и дату выюранного слота
        try {
            do {
                try {
                    WebElement dynamicElement = (new WebDriverWait(driver, 1))
                            .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-datetime='"+ dateANDtime + "']")));
                    if (dynamicElement.isDisplayed()){                                                                                   //Находим выбраный слот по локатору, если слот найден кликаем
                        dynamicElement.click();                                                                                          // по нему и разрываем цикл, если нет кликаем на стрелку дальше
                        break;                                                                                                           // и каждый раз проверяем есть ли элемент на странице
                    }
                }catch (Exception e){
                    driver.findElement(By.xpath("//*[@id=\"calendar\"]/div[2]/button[2]")).click();                                      //клик на стрелку дальше
                }
            }while (true);
        }catch (Exception ignored){}
    }

    public void fillTheForm() {                                                                                                                         //Заполняем форму бронирования, данные для формы берем из
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfAllElements(userPhone, userPromo, buttonRegister));
        room = roomName.getText();                                                                                                                       // класса TestData
        userPhone.sendKeys(phone);
        userPromo.sendKeys(name);
        buttonRegister.click();
        System.out.println("Забронировано: " + room +" ---- " + dateANDtime);                                                           //Выводим сообщение о том какая комната и на когда забронирована
        assertTrue(thankYou.isDisplayed());
    }

    public void fillTheFormOnlinePay () {
        System.out.println("-----ОНЛАЙН ОПЛАТА БРОНИРОВАНИЕ-----");                                                                                                                                //Заполняем форму бронирования, данные для формы берем из
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(paymentMethod));
        Select select = new Select(paymentMethod);
        select.selectByIndex(1);
        room = roomName.getText();                                                                                                                       // класса TestData
        userPhone.sendKeys(phone);
        userPromo.sendKeys(name);
        buttonRegister.click();
        System.out.println("Забронировано онлайн: " + room +" ---- " + dateANDtime);
    }

    public void waitForOnlinePayment () {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("LiqPay Dudka Agency"));
        String title = driver.getTitle();
        assertTrue(title.contains("LiqPay Dudka Agency"));
    }

    public void checkSms () {                                                                                                           //Проверяем смс, выбираем id, текст, номер телефона, статус смс
        try {
            Class.forName(dataBaseDriver);
            Connection conn = DriverManager.getConnection(dataBaseURL,
                    dataBaseLogin, dataBasePass);
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT * FROM logs_sms");

            while(rs.next()){
                smsID = rs.getInt("id");
                smsText = rs.getString("text");
                smsPhone = rs.getString("phone");
                smsStatus = rs.getString("status");
            }
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

        public void connectingToDatabase () {                                                                                           //Подключение к БД для последующих проверок
            try {                                                                                                                       // выбираем id бронирования для проверки что
                Class.forName(dataBaseDriver);                                                                                          // что новое бронирование создалось
                Connection conn = DriverManager.getConnection(dataBaseURL,
                        dataBaseLogin, dataBasePass);
                Statement stmt=conn.createStatement();
                ResultSet rs=stmt.executeQuery("SELECT * FROM reservation");

                while(rs.next()){
                    before = rs.getInt(1);
                }
                System.out.println(before);
                conn.close();
            }catch(Exception e){
                e.printStackTrace();
            }
        }

    public void connectingToDatabaseResult () {
        try {
            Class.forName(dataBaseDriver);
            Connection conn = DriverManager.getConnection(dataBaseURL,
                    dataBaseLogin, dataBasePass);
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT * FROM reservation");

            while(rs.next()){
                after = rs.getInt(1);
                assertDate = rs.getString(4);
                assertPhone = rs.getString("phone");
                assertSite = rs.getString("site");
                secretKey = rs.getString("secret_key");
                payment = rs.getString("payment");
                referer = rs.getString("http_referer");
            }
            System.out.println(after);
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void cancelReservation () {                                                                                              //Отмена всех созданых броней
        try {
            Class.forName(dataBaseDriver);
            Connection conn = DriverManager.getConnection(dataBaseURL,
                    dataBaseLogin, dataBasePass);
            Statement stmt=conn.createStatement();
            boolean rs=stmt.execute("UPDATE `reservation` SET `status` = 'canceled' WHERE `reservation`.`id` =" + after);
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void cancelAllReservation () {                                                                                              //Отмена всех созданых броней
        try {
            Class.forName(dataBaseDriver);
            Connection conn = DriverManager.getConnection(dataBaseURL,
                    dataBaseLogin, dataBasePass);
            Statement stmt=conn.createStatement();
            boolean rs=stmt.execute("UPDATE `reservation` SET `status` = 'canceled' WHERE `reservation`.`phone_formatted` = '"+ phone +"';");
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    //Проверки

    public void assertion () {
        try{
            assertNotEquals(before, after);
        }catch (AssertionFailedError error){
           error.printStackTrace();
        }
        try{
            assertEquals(dateANDtime, assertDate);
        }catch (AssertionFailedError error){
            error.printStackTrace();
        }
        try{
            assertEquals(phone, assertPhone);
        }catch (AssertionFailedError error){
            error.printStackTrace();
        }
        try{
            assertEquals("kadroom", assertSite);
        }catch (AssertionFailedError error){
            error.printStackTrace();
        }
        try{
            assertEquals(databaseSmsID+1, smsID);
        }catch (AssertionFailedError error){
            error.printStackTrace();
        }
        try{
            assertEquals(smsPhone, phone);
        }catch (AssertionFailedError error){
            error.printStackTrace();
        }
        try{
            assertEquals(smsStatus, "dev");
        }catch (AssertionFailedError error){
            error.printStackTrace();
        }
        try{
            boolean isContain = smsText.contains(secretKey);
            assertTrue(isContain);
        }catch (AssertionFailedError error){
            error.printStackTrace();
        }
    }

    public void assertionOnlinePay () {
        try{
            assertEquals(payment, "liqpay");
        }catch (AssertionFailedError error){
            error.printStackTrace();
        }
    }

    /*----------------ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ-------------*/



    public void selectPresale() {                                                                                                   //Выбираем Пресейл Тут спрятана логика выбора пресейла
        List<WebElement> rooms = driver.findElements(By.className("room-announce-btn"));                                            //Находим все пресейлы на странице
        int random = (int) (Math.random()*rooms.size());                                                                            //Рандомно выбираем один
        rooms.get(random).click();                                                                                                  //Кликаем
        userName.sendKeys(name);                                                                                                    //Вводим данные для бронирования пресейла
        userEmail.sendKeys(email);
        userTel.sendKeys(phone);
        submitButtonForm.submit();                                                                                                  //Отправляем форму
    }
}
