package com.kadroom.staging;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestBase implements TestData{

    public WebDriver driver;
    public WebDriverWait wait;

    public  String source;

    public int numberOfDate;

    public String attribute;

    public int num = 10;

    public String [] nameOfRoom = new String[num];
    public String [] roomId = new String[num];

    public int id;
    public int roomIdFromDatabase;
    public String sourceFromDatabase;
    public String phoneFromDatabase;

    @Before
    public void start () {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 20);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PageFactory.initElements(driver, this);
    }

    @After
    public void stop () {
        driver.quit();
    }

    public void openMultiBookingPage () {
        driver.navigate().to(multiBookingPage);
        source = sourcePage.getAttribute("value");
    }

    public void selectRooms(){
        List <WebElement> availableDates = driver.findElements(By.xpath("//div[@class='owl-item active'][*]"));
        numberOfDate = (int) (Math.random()*availableDates.size());
        if (numberOfDate == 0){
            numberOfDate++;
            WebElement selectDate = driver.findElement(By.xpath("//div[@class='owl-item active']["+ numberOfDate +"]"));
            selectDate.click();
        }else {
            WebElement selectDate = driver.findElement(By.xpath("//div[@class='owl-item active']["+ numberOfDate +"]"));
            selectDate.click();
        }

        for (int i = 0; i < num; i++) {
            wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//div[@class='info__schedule js-room']"))));
            List <WebElement> allRooms = driver.findElements(By.xpath("//div[@class='info__schedule js-room']"));
            int index = (int) (Math.random()*allRooms.size());

            nameOfRoom [i]= allRooms.get(index).getAttribute("data-name");
            roomId [i] = allRooms.get(index).getAttribute("data-room_id");

            List <WebElement> timeOfGame = driver.findElements(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[*]"));
            for (WebElement webElement : timeOfGame) {
                attribute = webElement.getAttribute("class");
                if (attribute.contains("available") & !attribute.contains("selected")){
                    List <WebElement> availableTimeOfGame = driver.findElements(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[@class='"+ attribute +"']"));
                    index = (int) (Math.random()*availableTimeOfGame.size());
                    if (index == 0) {
                        index++;
                        try{
                            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[@class='"+ attribute +"']["+ index +"]"))));
                            WebElement element = driver.findElement(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[@class='"+ attribute +"']["+ index +"]"));
                            Actions actions = new Actions(driver);
                            actions.moveToElement(element).click().build().perform();
                            break;
                        }catch (Exception e){
                            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[@class='"+ attribute +"']["+ index +"]"))));
                            WebElement element = driver.findElement(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[@class='calendar-item calendar__item disable-for-admin available actor_2']["+ index +"]"));
                            Actions actions = new Actions(driver);
                            actions.moveToElement(element).click().build().perform();
                            break;
                        }
                    }else {
                        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[@class='"+ attribute +"']["+ index +"]"))));
                        WebElement element = driver.findElement(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[@class='"+ attribute +"']["+ index +"]"));
                        Actions actions = new Actions(driver);
                        actions.moveToElement(element).click().build().perform();
                        break;
                    }
                }
            }
            System.out.println(nameOfRoom[i] + "   " + roomId[i]);
        }
        submitButton.click();
    }

    public void fillTheForm () {
        userName.sendKeys(name);

        userPhone.sendKeys(phone);

        userEmail.sendKeys(email);

        userText.sendKeys(text);

        orderButton.click();
    }

    public void connectingToDatabase () {
        for (int i = 0; i < num; i++) {
            try {
                Class.forName(dataBaseDriver);
                Connection conn = DriverManager.getConnection(dataBaseURL,
                        dataBaseLogin, dataBasePass);
                Statement stmt=conn.createStatement();
                ResultSet rs=stmt.executeQuery("SELECT * FROM `reservation` WHERE `source_page`= '"+ source +"' AND `room_id`= "+ roomId[i] +" AND `phone` = "+ phone +" AND `status` = 'new'");

                while(rs.next()){
                    id = rs.getInt(1);
                    roomIdFromDatabase = rs.getInt(2);
                    sourceFromDatabase = rs.getString("source_page");
                    phoneFromDatabase = rs.getString("phone");
                }

                int update = stmt.executeUpdate("UPDATE `reservation` SET `status` = 'canceled' WHERE `reservation`.`id` = "+ id +";");

                conn.close();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    @FindBy (name = "source_page")
    public WebElement sourcePage;

    @FindBy (xpath = "//input[@class='action-button'][@value='К оформлению']")
    public WebElement submitButton;

    @FindBy (name = "name")
    public WebElement userName;

    @FindBy (name = "phone")
    public WebElement userPhone;

    @FindBy (name = "email")
    public WebElement userEmail;

    @FindBy (xpath = "//input[4]")
    public WebElement userText;

    @FindBy (xpath = "//input[@class='action-button'][@value='Забронировать']")
    public WebElement orderButton;

}