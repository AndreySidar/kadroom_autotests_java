import Pages.*;
import TestData.TestData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public class MainTest implements TestData {

    //СТАРТ

    private WebDriver driver;

    /*---------ИНИЦИАЛИЗАЦИЯ ДРАЙВЕРА---------
     *        Происходит до вызова класса       */

    @Before
    public void start() {
        System.setProperty("webdriver.chrome.driver", "chromedriver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        PageFactory.initElements(driver, this);
    }

    //---------ПОСЛЕ ВЫПОЛНЕНИЯ ТЕСТОВ ВЫХОД ИЗ ДРАЙВЕРА---------//

    @After
    public void stop() {
        driver.quit();
    }

    /*----------ТЕСТИРОВАНИЕ ФОРМ ОБРАТНОЙ СВЯЗИ----------*/

    //ТЕСТИРОАНИЕ ФОРМ ГЛАВНАЯ СТРАНИЦА

      @Test
      public void mainPageTest () throws InterruptedException {

          MainPage mainPage = new MainPage(driver);



          /* ----------ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ--------- */

        //ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ РУС ВЕРСИЯ

        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ РУС ВЕРСИЯ----");

        mainPage.openMainPage();
        mainPage.selectPresale();

        //ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ АНГЛ ВЕРСИЯ

        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ АНГЛ ВЕРСИЯ----");

        mainPage.openMainPageEn();
        mainPage.selectPresale();

        //ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ УКР ВЕРСИЯ

        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ПРЕСЕЙЛОВ УКР ВЕРСИЯ----");

        mainPage.openMainPageUa();
        mainPage.selectPresale();

        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ФОРМ ГЛАВНАЯ СТРАНИЦА----");


        mainPage.openMainPage();
        mainPage.fillForm();

        mainPage.openMainPageEn();
        mainPage.fillForm();

        mainPage.openMainPageUa();
        mainPage.fillForm();

        mainPage.openMainPage();
        mainPage.incorrectFillForm();

        mainPage.openMainPage();
        mainPage.emptyFillForm();
    }

    //ТЕСТИРОАНИЕ ФОРМ СТРАНИЦА КОНТАКТЫ

    @Test
    public void contactPageTest () {
        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ФОРМ СТРАНИЦА КОНТАКТЫ----");

        ContactPage contactPage = new ContactPage(driver);

        contactPage.openContactPage();
        contactPage.fillForm();

        contactPage.openContactPageEn();
        contactPage.fillForm();

        contactPage.openContactPageUa();
        contactPage.fillForm();

        System.out.println("\n");
        System.out.println("----НЕГАТИВНОЕ ТЕСТИРОВАНИЕ ФОРМ----");

        contactPage.openContactPage();
        contactPage.emptyFillForm();

        contactPage.openContactPage();
        contactPage.incorrectFillForm();
    }

    @Test
    public void corporatePAgqTest () {
        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ФОРМ КОРПОРАТИВНАЯ СТРАНИЦА----");

        CorporatePage corporatePage = new CorporatePage(driver);

        corporatePage.openCorporatePage();
        corporatePage.fillForm();

        corporatePage.openCorporatePage();
        corporatePage.emptyFillForm();
        corporatePage.openCorporatePage();
        corporatePage.incorrectFillForm();

    }

    @Test
    public void franchisePageTest () {
        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ФОРМ СТРАНИЦА ФРАНШИЗЫ----");

        FranchisePage franchisePage = new FranchisePage(driver);

        franchisePage.openFranchisePage();
        franchisePage.fillForm();

        franchisePage.openFranchisePageEn();
        franchisePage.fillForm();

        franchisePage.openFranchisePageUa();
        franchisePage.fillForm();

        franchisePage.openFranchisePage();
        franchisePage.emptyFillForm();

        franchisePage.openFranchisePage();
        franchisePage.incorrectFillForm();
    }

    @Test
    public void faqPageTest () {
        System.out.println("\n");
        System.out.println("----ТЕСТИРОВАНИЕ ФОРМ СТРАНИЦА FAQ----");

        FaqPage faqPage = new FaqPage(driver);

        faqPage.openFaqPage();
        faqPage.fillForm();

        faqPage.openFaqPageEn();
        faqPage.fillForm();

        faqPage.openFaqPageUa();
        faqPage.fillForm();

        faqPage.openFaqPage();
        faqPage.emptyFillForm();

        faqPage.openFaqPage();
        faqPage.incorrectFillForm();
    }
    /*@Test
    public void certificatePageTest() {

        CertificatePage certificatePage = new CertificatePage(driver);
    }*/

}
