package Pages;

import Entites.FormTest;
import TestData.Locators;
import TestData.TestData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static junit.framework.TestCase.assertTrue;

public class CertificatePage extends Locators implements TestData {

    private WebDriver driver;

    public String typeOfCertificate;

    public String nameOfCertificate;

    public CertificatePage(WebDriver driver) {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
        this.driver = driver;
    }

    public void open() {
        driver.navigate().to(certificateURL);
    }

    public void openEn() {
        driver.navigate().to(certificateURLEn);
    }

    public void openUa() {
        driver.navigate().to(certificateURLUa);
    }

}
