package Pages;

import Entites.FormTest;
import TestData.TestData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ContactPage implements TestData {

    private WebDriver driver;

    public ContactPage(WebDriver driver) {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
        this.driver = driver;
    }

    public void openContactPage() {
        driver.navigate().to(contactURL);
    }

    public void openContactPageEn() {
        driver.navigate().to(contactURLEn);
    }

    public void openContactPageUa() {
        driver.navigate().to(contactURLUa);
    }

    public void fillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.fillTheForm();
    }

    public void incorrectFillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.incorrectFillTheForm();
        formTest.incorrectNumberTrue();
    }

    public void emptyFillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.emptyFillTheForm();
        formTest.emptyFormTrue();
    }
}
