package com.kadroom;

public class TestSteps extends TestBase{
    public void PresaleTest () {
        openMainPage();
        selectPresale();
        presaleEmptyValidation();
        presaleInvalidPhoneValidation();
        presaleInvalidEmailValidation();
    }
    public void PresaleTestEn () {
        openMainPageEn();
        selectPresaleEn();

    }
    public void PresaleTestUa () {
        openMainPageUa();
        selectPresaleUa();
    }
}
