package Tests;

import Data.TestData;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class FormsTest implements TestData {

    private WebDriver driver;

    @FindBy(xpath = "//span[@class='writeus-title']")
    public WebElement writeUs;

    @FindBy(name = "text-104")
    public WebElement userName;

    @FindBy(name = "tel-193")
    public WebElement userPhone;

    @FindBy(name = "email-795")
    public WebElement userEmail;

    @FindBy(name = "textarea-726")
    public WebElement userText;

    @FindBy(xpath = "//input[@type='submit']")
    public WebElement submitButtonForm;

    @FindBy(xpath = "//button")
    public WebElement button;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement incorrectNumber;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement emptyForm;

    public FormsTest (WebDriver driver){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
        this.driver = driver;
    }

    /*---------ОТКРЫТИЕ ГЛАВНОЙ СТРАНИЦИ---------*/

    public void openMainPage () {
        driver.navigate().to(mainUrl);
    }
    public void openMainPageEn () {
        driver.navigate().to(mainUrlEn);
    }
    public void openMainPageUa () {
        driver.navigate().to(mainUrlUa);
    }

    /*---------ОТКРЫТИЕ СТРАНИЦИ КОНТАКТЫ---------*/

    public void openContactPage () {
        driver.navigate().to(contactURL);
    }
    public void openContactPageEn () {
        driver.navigate().to(contactURLEn);
    }
    public void openContactPageUa () {
        driver.navigate().to(contactURLUa);
    }


    public void openCorporatePage () {
        driver.navigate().to(corporateURL);
    }


    public void openFranchisePage () {
        driver.navigate().to(franchiseURL);
    }
    public void openFranchisePageEn () {
        driver.navigate().to(franchiseURLEn);
    }
    public void openFranchisePageUa () {
        driver.navigate().to(franchiseURLUa);
    }

    public void writeUsLableClick () {
        writeUs.click();
    }

    public void buttonClick () {
        button.click();
    }

    public void fillTheForm () {
        Actions actions = new Actions(driver);

        actions.sendKeys(userName, name);
        actions.sendKeys(userPhone, phone);
        actions.sendKeys(userEmail, email);
        actions.sendKeys(userText, text);

        actions.click(submitButtonForm).build().perform();
    }

    public void incorrectFillTheForm () {
        Actions actions = new Actions(driver);

        actions.sendKeys(userName, name);
        actions.sendKeys(userPhone, name);
        actions.sendKeys(userEmail, email);
        actions.sendKeys(userText, text);

        actions.click(submitButtonForm).build().perform();
    }

    public void emptyFillTheForm () {
        submitButtonForm.click();
    }

    public void incorrectNumberTrue () {
        Assert.assertTrue("Некорректный номер телефона.", incorrectNumber.isDisplayed());
    }

    public void emptyFormTrue () {
        Assert.assertTrue("Пожалуйста, укажите телефон или email!", incorrectNumber.isDisplayed());
    }
}
