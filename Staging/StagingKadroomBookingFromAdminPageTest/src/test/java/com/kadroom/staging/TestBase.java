package com.kadroom.staging;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestBase implements TestData{

    public WebDriver driver;
    public WebDriverWait wait;

    public String dateAndTime;
    public int index;

    public String dataId;
    public int idFromDatabase;

    public String statusValue;
    public String sourceValue;
    public String paymentValue;
    public String discountValue;

    public String statusFromDatabase;
    public String sourceFromDatabase;
    public String paymentFromDatabase;
    public String discountFromDatabase;
    public String commentFromDatabase;


    public List <WebElement> availableRooms;

    @Before
    public void start () {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 20);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        PageFactory.initElements(driver, this);
    }

    @After
    public void stop () {
        driver.quit();
    }

    public void openAdminPage () {
        driver.navigate().to(adminURL);
        inputLog.click();
        inputLog.sendKeys(adminLog);
        inputPass.click();
        inputPass.sendKeys(adminPass);
        submitButton.submit();
    }

    public void openModerateRooms () {
        moderateRooms.click();
    }

    public void selectRoom () {
        availableRooms = driver.findElements(By.xpath("//div[@data-datetime][@data-id='']"));
        index = (int) (Math.random()*availableRooms.size());
        dateAndTime = availableRooms.get(index).getAttribute("data-datetime");
        try{
            do {
                try{
                    WebElement dynamicElement = (new WebDriverWait(driver, 1))
                        .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-datetime='"+ dateAndTime + "'][@data-id='']")));
                if (dynamicElement.isDisplayed()){
                    dynamicElement.click();
                    break;
                }
            }catch (Exception e){
                    buttonNext.click();
            }
        }while (true);
    }catch (Exception ignored){}
    }

    public void fillForm () {
        wait.until(ExpectedConditions.visibilityOfAllElements(inputPhone, inputEmail, inputPromo, inputName, inputComments));

        Select select = new Select(discount);
        List options = select.getOptions();
        int index = (int) (Math.random() * options.size());
        select.selectByIndex(index);
        discountValue = discount.getAttribute("value");

        inputPhone.click();
        inputPhone.sendKeys(phone);
        inputEmail.click();
        inputEmail.sendKeys(email);
        inputPromo.click();
        inputPromo.sendKeys(promo);
        inputName.click();
        inputName.sendKeys(name);
        inputComments.click();
        inputComments.sendKeys(textAdmin);

        select = new Select(status);
        options = select.getOptions();
        index = (int) (Math.random() * options.size());
        select.selectByIndex(index);
        statusValue = status.getAttribute("value");

        select = new Select(source);
        options = select.getOptions();
        index = (int) (Math.random() * options.size());
        select.selectByIndex(index);
        sourceValue = source.getAttribute("value");

        select = new Select(payment);
        options = select.getOptions();
        index = (int) (Math.random() * options.size());
        select.selectByIndex(index);
        paymentValue = payment.getAttribute("value");

        buttonRegister.click();

        dataId = driver.findElement(By.xpath("//div[@data-datetime='" + dateAndTime + "'][@data-status='"+ statusValue + "']")).getAttribute("data-id");

        if (discountValue.contains("200")){
            discountValue="dr";
        }else if (discountValue.contains("150")){
            discountValue="combo";
        }else{
            discountValue=" ";
        }
    }

    public void cancelFromDatabase () {
        try {
            Class.forName(dataBaseDriver);
            Connection conn = DriverManager.getConnection(dataBaseURL,
                    dataBaseLogin, dataBasePass);
            Statement stmt=conn.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT * FROM `reservation` WHERE `id`='" + dataId + "'");

            while(resultSet.next()){
                idFromDatabase = resultSet.getInt(1);
                statusFromDatabase = resultSet.getString("status");
                sourceFromDatabase = resultSet.getString("source");
                paymentFromDatabase = resultSet.getString("payment");
                discountFromDatabase = resultSet.getString("discount");
                commentFromDatabase = resultSet.getString("comment");
            }

            int rs=stmt.executeUpdate("UPDATE `reservation` SET `status` = 'canceled' WHERE `reservation`.`id` ='" + dataId + "'");

            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void assertion () {
        Assert.assertTrue(dataId.contains(String.valueOf(idFromDatabase)));
        Assert.assertEquals(sourceValue, sourceFromDatabase);
        Assert.assertEquals(sourceValue, sourceFromDatabase);
        Assert.assertEquals(paymentValue, paymentFromDatabase);
        Assert.assertEquals(discountValue, discountFromDatabase);
        Assert.assertEquals(textAdmin, commentFromDatabase);
    }

    @FindBy(name = "log")
    public WebElement inputLog;

    @FindBy(name = "pwd")
    public WebElement inputPass;

    @FindBy(name = "wp-submit")
    public WebElement submitButton;


    @FindBy(id = "wp-admin-bar-custom-moderate-rooms")
    public WebElement moderateRooms;

    @FindBy(className = "owl-next")
    public WebElement buttonNext;
    @FindBy(className = "owl-prev")
    public WebElement buttonPrev;


    @FindBy(name = "phone")
    public WebElement inputPhone;
    @FindBy(name = "email")
    public WebElement inputEmail;
    @FindBy(name = "promo")
    public WebElement inputPromo;
    @FindBy(name = "name")
    public WebElement inputName;
    @FindBy(name = "comments")
    public WebElement inputComments;

    @FindBy(name = "discount_percent_value_select")
    public WebElement discount;
    @FindBy(name = "status")
    WebElement status;
    @FindBy(name = "source")
    public WebElement source;
    @FindBy(name = "payment")
    public WebElement payment;

    @FindBy(id = "register")
    public WebElement buttonRegister;

    @FindBy(className = "container__close")
    public WebElement close;
}