package com.kadroom.staging;

public class TestSteps extends TestBase{
    public void BookingFromAdminPageTest () {
        openAdminPage();
        openModerateRooms();
        selectRoom();
        fillForm();
        cancelFromDatabase();
        assertion();
    }
}
