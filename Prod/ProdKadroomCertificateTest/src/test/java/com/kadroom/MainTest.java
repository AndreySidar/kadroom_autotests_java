package com.kadroom;

import org.junit.Test;

public class MainTest extends TestSteps{

    @Test
    public void Test () {
        TestCertificatePage();
        TestCertificatePageEn();
        TestCertificatePageUa();
        IncorrectNumberTest();
        EmptyFormTest();
    }
}
