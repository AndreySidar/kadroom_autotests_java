package com.kadroom;

public class TestSteps extends TestBase{

    public void TestMainPage () {
        openMainPage();
        writeUsLableClick();
        fillTheForm();
        openMainPageEn();
        writeUsLableClick();
        fillTheForm();
        openMainPageUa();
        writeUsLableClick();
        fillTheForm();
    }
    public void TestContactPage () {
        openContactPage();
        fillTheForm();
        openContactPageEn();
        fillTheForm();
        openContactPageUa();
        fillTheForm();
    }
    public void TestCorporatePage () {
        openCorporatePage();
        buttonClick();
        fillTheForm();
    }
    public void TestFranchisePage () {
        openFranchisePage();
        buttonClick();
        fillTheForm();
        openFranchisePageEn();
        buttonClick();
        fillTheForm();
        openFranchisePageUa();
        buttonClick();
        fillTheForm();
    }

    public void IncorrectNumberTest () {
        openMainPage();
        writeUsLableClick();
        incorrectFillTheForm();
        incorrectNumberTrue();

        openContactPage();
        incorrectFillTheForm();
        incorrectNumberTrue();

        openFranchisePage();
        buttonClick();
        incorrectFillTheForm();
        incorrectNumberTrue();

        openCorporatePage();
        buttonClick();
        incorrectFillTheForm();
        incorrectNumberTrue();
    }
    public void EmptyFormPage () {
        openMainPage();
        writeUsLableClick();
        emptyFillTheForm();
        emptyFormTrue();

        openContactPage();
        emptyFillTheForm();
        emptyFormTrue();

        openFranchisePage();
        buttonClick();
        emptyFillTheForm();
        emptyFormTrue();

        openCorporatePage();
        buttonClick();
        emptyFillTheForm();
        emptyFormTrue();
    }
}
