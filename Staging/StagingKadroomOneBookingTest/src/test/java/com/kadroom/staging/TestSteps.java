package com.kadroom.staging;

public class TestSteps extends TestBase{
    public void OneBookingTest () {
        openMainPage();
        selectRoom();
        selectGameTime();
        connectingToDatabase();
        fillTheForm();
        connectingToDatabaseResult();
        assertion();
        cancelReservation();
        emptyFillForm();
        invalidFillForm();
    }
    public void OneBookingTestEn () {
        openMainPageEn();
        selectRoom();
        selectGameTime();
        connectingToDatabase();
        fillTheForm();
        connectingToDatabaseResult();
        assertion();
        cancelReservation();
    }
    public void OneBookingTestUa () {
        openMainPageUa();
        selectRoom();
        selectGameTime();
        connectingToDatabase();
        fillTheForm();
        connectingToDatabaseResult();
        assertion();
        cancelReservation();
    }
}
