package Tests;

import Data.TestData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class AdminBookingTest implements TestData {

    private WebDriver driver;

    public String dateAndTime;
    public int index;

    public String dataId;
    public int idFromDatabase;

    public String statusValue;
    public String sourceValue;
    public String paymentValue;
    public String discountValue;

    public String statusFromDatabase;
    public String sourceFromDatabase;
    public String paymentFromDatabase;
    public String discountFromDatabase;
    public String commentFromDatabase;

    public List<WebElement> availableRooms;

    @FindBy(name = "log")
    public WebElement inputLog;

    @FindBy(name = "pwd")
    public WebElement inputPass;

    @FindBy(name = "wp-submit")
    public WebElement submitButton;


    @FindBy(id = "wp-admin-bar-custom-moderate-rooms")
    public WebElement moderateRooms;

    @FindBy(className = "owl-next")
    public WebElement buttonNext;
    @FindBy(className = "owl-prev")
    public WebElement buttonPrev;


    @FindBy(name = "phone")
    public WebElement inputPhone;
    @FindBy(name = "email")
    public WebElement inputEmail;
    @FindBy(name = "promo")
    public WebElement inputPromo;
    @FindBy(name = "name")
    public WebElement inputName;
    @FindBy(name = "comments")
    public WebElement inputComments;

    @FindBy(name = "discount_percent_value_select")
    public WebElement discount;
    @FindBy(name = "status")
    public WebElement status;
    @FindBy(name = "source")
    public WebElement source;
    @FindBy(name = "payment")
    public WebElement payment;

    @FindBy(id = "register")
    public WebElement buttonRegister;

    @FindBy(className = "container__close")
    public WebElement close;

    public AdminBookingTest (WebDriver driver){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
        this.driver = driver;
    }

    public void openAdminPage () {
        driver.navigate().to(adminURL);
        inputLog.click();
        inputLog.sendKeys(adminLog);
        inputPass.click();
        inputPass.sendKeys(adminPass);
        submitButton.submit();
    }

    public void openModerateRooms () {
        moderateRooms.click();
    }

    public void selectRoom () {
        availableRooms = driver.findElements(By.xpath("//div[@data-datetime][@data-id='']"));
        index = (int) (Math.random()*availableRooms.size());
        dateAndTime = availableRooms.get(index).getAttribute("data-datetime");
        buttonPrev.click();
        try{
            do {
                try{
                    WebElement dynamicElement = (new WebDriverWait(driver, 1))
                            .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-datetime='"+ dateAndTime + "'][@data-id='']")));
                    if (dynamicElement.isDisplayed()){
                        dynamicElement.click();
                        break;
                    }
                }catch (Exception e){
                    buttonNext.click();
                }
            }while (true);
        }catch (Exception ignored){}
    }

    public void fillForm () {

        Select select = new Select(discount);
        List options = select.getOptions();
        int index = (int) (Math.random() * options.size());
        select.selectByIndex(index);
        discountValue = discount.getAttribute("value");

        Actions actions = new Actions(driver);

        actions.moveToElement(inputPhone).click().build().perform();
        actions.sendKeys(inputPhone, adminPhone);

        actions.moveToElement(inputEmail).click().build().perform();
        actions.sendKeys(inputEmail, adminEmail);

        actions.moveToElement(inputPromo).click().build().perform();
        actions.sendKeys(inputPromo, adminPromo);

        actions.moveToElement(inputName).click().build().perform();
        actions.sendKeys(inputName, adminName);

        actions.moveToElement(inputComments).click().build().perform();
        actions.sendKeys(inputComments, textAdmin);

        select = new Select(status);
        options = select.getOptions();
        index = (int) (Math.random() * options.size());
        select.selectByIndex(index);
        statusValue = status.getAttribute("value");

        select = new Select(source);
        options = select.getOptions();
        index = (int) (Math.random() * options.size());
        select.selectByIndex(index);
        sourceValue = source.getAttribute("value");

        select = new Select(payment);
        options = select.getOptions();
        index = (int) (Math.random() * options.size());
        select.selectByIndex(index);
        paymentValue = payment.getAttribute("value");

        actions.moveToElement(buttonRegister).click().build().perform();

        dataId = driver.findElement(By.xpath("//div[@data-datetime='" + dateAndTime + "'][@data-status='"+ statusValue + "']")).getAttribute("data-id");

        if (discountValue.contains("200")){
            discountValue="dr";
        }else if (discountValue.contains("150")){
            discountValue="combo";
        }else{
            discountValue="";
        }
    }

    public void cancelFromDatabase () {
        try {
            Class.forName(dataBaseDriver);
            Connection conn = DriverManager.getConnection(dataBaseURL,
                    dataBaseLogin, dataBasePass);
            Statement stmt=conn.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT * FROM `reservation` WHERE `id`='" + dataId + "'");

            while(resultSet.next()){
                idFromDatabase = resultSet.getInt(1);
                statusFromDatabase = resultSet.getString("status");
                sourceFromDatabase = resultSet.getString("source");
                paymentFromDatabase = resultSet.getString("payment");
                discountFromDatabase = resultSet.getString("discount");
                commentFromDatabase = resultSet.getString("comment");
            }

            int rs=stmt.executeUpdate("UPDATE `reservation` SET `status` = 'canceled' WHERE `reservation`.`id` ='" + dataId + "'");

            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void assertion () {
        assertTrue(dataId.contains(String.valueOf(idFromDatabase)));
        assertEquals(sourceValue, sourceFromDatabase);
        assertEquals(sourceValue, sourceFromDatabase);
        assertEquals(paymentValue, paymentFromDatabase);
        assertEquals(discountValue, discountFromDatabase);
        assertEquals(textAdmin, commentFromDatabase);
    }
}
