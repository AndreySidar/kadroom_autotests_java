package com.kadroom;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class TestBase implements TestData{

    public WebDriver driver;
    public WebDriverWait wait;

    @Before
    public void start () {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 20);
        PageFactory.initElements(driver, this);
    }

    @After
    public void stop () {
        driver.quit();
    }

    public void openMainPage () {
        driver.navigate().to(mainURL);
    }
    public void openMainPageEn () {
        driver.navigate().to(mainURLEn);
    }
    public void openMainPageUa () {
        driver.navigate().to(mainURLUa);
    }

    public void selectPresale () {
        List<WebElement> rooms = driver.findElements(By.className("room-announce-btn"));
        int random = (int) (Math.random()*rooms.size());
        rooms.get(random).click();
        userName.sendKeys(name);
        userEmail.sendKeys(email);
        userTel.sendKeys(tel);
        submitButtonForm.submit();
        wait.until(ExpectedConditions.visibilityOf(alertOK));
        Assert.assertTrue(alertOK.isDisplayed());
        Assert.assertEquals(alertOK.getText(), "Ваша заявка принята. Спасибо!");
    }
    public void selectPresaleEn () {
        List<WebElement> rooms = driver.findElements(By.className("room-announce-btn"));
        int random = (int) (Math.random()*rooms.size());
        rooms.get(random).click();
        userName.sendKeys(name);
        userEmail.sendKeys(email);
        userTel.sendKeys(tel);
        submitButtonForm.submit();
        wait.until(ExpectedConditions.visibilityOf(alertOK));
        Assert.assertTrue(alertOK.isDisplayed());
        Assert.assertEquals(alertOK.getText(), "Your application is accepted. Thank!");
    }
    public void selectPresaleUa () {
        List<WebElement> rooms = driver.findElements(By.className("room-announce-btn"));
        int random = (int) (Math.random()*rooms.size());
        rooms.get(random).click();
        userName.sendKeys(name);
        userEmail.sendKeys(email);
        userTel.sendKeys(tel);
        submitButtonForm.submit();
        wait.until(ExpectedConditions.visibilityOf(alertOK));
        Assert.assertTrue(alertOK.isDisplayed());
        Assert.assertEquals(alertOK.getText(), "Ваша заявка прийнята. Дякуємо!");
    }

    public void presaleEmptyValidation () {
        openMainPage();
        List<WebElement> rooms = driver.findElements(By.className("room-announce-btn"));
        int random = (int) (Math.random()*rooms.size());
        rooms.get(random).click();
        submitButtonForm.submit();
        wait.until(ExpectedConditions.visibilityOf(alertError));
        Assert.assertTrue(alertError.isDisplayed());
        Assert.assertEquals(alertError.getText(), "Ошибки заполнения. Пожалуйста, проверьте все поля и отправьте снова.");
    }

    public void presaleInvalidPhoneValidation () {
        for (int i = 0; i < invalidPhone.length; i++) {
            openMainPage();
            List<WebElement> rooms = driver.findElements(By.className("room-announce-btn"));
            int random = (int) (Math.random()*rooms.size());
            rooms.get(random).click();
            userName.sendKeys(name);
            userTel.sendKeys(invalidPhone[i]);
            userEmail.sendKeys(email);
            submitButtonForm.click();
            wait.until(ExpectedConditions.invisibilityOf(loader));
            try{
                if (alertOK.isDisplayed()){
                    wait.until(ExpectedConditions.visibilityOf(alertOK));
                    System.out.println("Вадидация не работает! " + invalidPhone[i]);
                }else {

                }
            }catch (Exception e){
                wait.until(ExpectedConditions.visibilityOf(alertError));
                Assert.assertTrue(alertError.isDisplayed());
                Assert.assertEquals(alertError.getText(), "Ошибки заполнения. Пожалуйста, проверьте все поля и отправьте снова.");
                Assert.assertTrue(alert.isDisplayed());
                Assert.assertEquals(alert.getText(), "Некорректный номер телефона.");
            }
        }
    }
    public void presaleInvalidEmailValidation () {
        for (int i = 0; i < invalidEmail.length; i++) {
            openMainPage();
            List<WebElement> rooms = driver.findElements(By.className("room-announce-btn"));
            int random = (int) (Math.random()*rooms.size());
            rooms.get(random).click();
            userName.sendKeys(name);
            userTel.sendKeys(tel);
            userEmail.sendKeys(invalidEmail[i]);
            submitButtonForm.submit();
            wait.until(ExpectedConditions.invisibilityOf(loader));
            try{
                if (alertOK.isDisplayed()){
                    wait.until(ExpectedConditions.visibilityOf(alertOK));
                    System.out.println("Вадидация не работает! " + invalidEmail[i]);
                }else {

                }
            }catch (Exception e){
                wait.until(ExpectedConditions.visibilityOf(alertError));
                Assert.assertTrue(alertError.isDisplayed());
                Assert.assertEquals(alertError.getText(), "Ошибки заполнения. Пожалуйста, проверьте все поля и отправьте снова.");
                Assert.assertTrue(alert.isDisplayed());
                Assert.assertEquals(alert.getText(), "Некорректный e-mail.");
            }
        }
    }

    @FindBy(name = "text-104")
    public WebElement userName;

    @FindBy(name = "tel-193")
    public WebElement userTel;

    @FindBy(name = "email-795")
    public WebElement userEmail;

    @FindBy(xpath = "//input[@type='submit']")
    public WebElement submitButtonForm;

    @FindBy(className = "wpcf7-mail-sent-ok")
    public WebElement alertOK;

    @FindBy(className = "wpcf7-validation-errors")
    public WebElement alertError;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement alert;

    @FindBy(className = "ajax-loader")
    public WebElement loader;
}
