package com.kadroom;

public interface TestData {
    String certificateURL = "https://kadroom.com/cert/";
    String certificateURLEn = "https://kadroom.com/en/cert/";
    String certificateURLUa = "https://kadroom.com/uk/cert/";

    String all_inclusive = "all_inclusive";
    String quest_room = "quest_room";
    String cost = "cost";

    String name = "test";
    String tel = "0933070545";
    String email = "ss60600@gmail.com";
    String text = "Hello from Dudka Agency!\n This is Test...\n We are sorry)";
}
