package Pages;

import Data.TestData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

public class BookingPage implements TestData {

    private WebDriver driver;

    public   String source;

    public int numberOfDate;

    public String attribute;

    public int num = 5;

    public String [] nameOfRoom = new String[num];
    public String [] roomId = new String[num];

    public int id;
    public int roomIdFromDatabase;
    public String sourceFromDatabase;
    public String phoneFromDatabase;

    @FindBy(name = "source_page")
    public WebElement sourcePage;

    @FindBy (xpath = "//div[3]/input[@class='action-button']")
    public WebElement submitButton;

    @FindBy (name = "name")
    public WebElement userName;

    @FindBy (name = "phone")
    public WebElement userPhone;

    @FindBy (name = "email")
    public WebElement userEmail;

    @FindBy (xpath = "//input[4]")
    public WebElement userText;

    @FindBy (xpath = "//div[1]/input[@class='action-button']")
    public WebElement orderButton;

    public BookingPage (WebDriver driver){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
        this.driver = driver;
    }

    public void open () {
        driver.navigate().to(multiBookingPage);
        source = sourcePage.getAttribute("value");
    }
    public void openEn () {
        driver.navigate().to(multiBookingPageEn);
        source = sourcePage.getAttribute("value");
    }
    public void openUa () {
        driver.navigate().to(multiBookingPageUa);
        source = sourcePage.getAttribute("value");
    }

    public void selectRooms(){
        List<WebElement> availableDates = driver.findElements(By.xpath("//div[@class='owl-item active'][*]"));
        numberOfDate = (int) (Math.random()*availableDates.size());
        if (numberOfDate == 0){
            numberOfDate++;
            WebElement selectDate = driver.findElement(By.xpath("//div[@class='owl-item active']["+ numberOfDate +"]"));
            Actions actions = new Actions(driver);
            actions.moveToElement(selectDate).click().build().perform();
        }else {
            WebElement selectDate = driver.findElement(By.xpath("//div[@class='owl-item active']["+ numberOfDate +"]"));
            Actions actions = new Actions(driver);
            actions.moveToElement(selectDate).click().build().perform();
        }

        for (int i = 0; i < num; i++) {
            List <WebElement> allRooms = driver.findElements(By.xpath("//div[@class='info__schedule js-room']"));
            int index = (int) (Math.random()*allRooms.size());

            nameOfRoom [i]= allRooms.get(index).getAttribute("data-name");
            roomId [i] = allRooms.get(index).getAttribute("data-room_id");

            List <WebElement> timeOfGame = driver.findElements(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[*]"));
            for (WebElement webElement : timeOfGame) {
                attribute = webElement.getAttribute("class");
                if (attribute.contains("available") & !attribute.contains("selected")){
                    List <WebElement> availableTimeOfGame = driver.findElements(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[@class='"+ attribute +"']"));
                    index = (int) (Math.random()*availableTimeOfGame.size());
                    if (index == 0) {
                        index++;
                        try{
                            WebElement element = driver.findElement(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[@class='"+ attribute +"']["+ index +"]"));
                            Actions actions = new Actions(driver);
                            actions.click(element).build().perform();
                            break;
                        }catch (Exception e){
                            WebElement element = driver.findElement(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[@class='calendar-item calendar__item disable-for-admin available actor_2']["+ index +"]"));
                            Actions actions = new Actions(driver);
                            actions.click(element).build().perform();
                            break;
                        }
                    }else {
                        WebElement element = driver.findElement(By.xpath("//div[@data-room_id='"+ roomId[i] +"']/li[@class='"+ attribute +"']["+ index +"]"));
                        Actions actions = new Actions(driver);
                        actions.click(element).build().perform();
                        break;
                    }
                }
            }
            System.out.println(nameOfRoom[i] + "   " + roomId[i]);
        }
        submitButton.click();
    }

    public void fillTheForm () {
        userName.clear();
        userName.sendKeys(name);

        userPhone.clear();
        userPhone.sendKeys(phone);

        userEmail.clear();
        userEmail.sendKeys(email);

        userText.clear();
        userText.sendKeys(text);

        orderButton.click();
    }

    public void connectingToDatabase () {
        for (int i = 0; i < num; i++) {
            try {
                Class.forName(dataBaseDriver);
                Connection conn = DriverManager.getConnection(dataBaseURL,
                        dataBaseLogin, dataBasePass);
                Statement stmt=conn.createStatement();
                ResultSet rs=stmt.executeQuery("SELECT * FROM `reservation` WHERE `source_page`= '"+ source +"' AND `room_id`= "+ roomId[i] +" AND `phone` = "+ phone +" AND `status` = 'new'");

                while(rs.next()){
                    id = rs.getInt(1);
                    roomIdFromDatabase = rs.getInt(2);
                    sourceFromDatabase = rs.getString("source_page");
                    phoneFromDatabase = rs.getString("phone");
                }

                int update = stmt.executeUpdate("UPDATE `reservation` SET `status` = 'canceled' WHERE `reservation`.`id` = "+ id +";");

                conn.close();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
