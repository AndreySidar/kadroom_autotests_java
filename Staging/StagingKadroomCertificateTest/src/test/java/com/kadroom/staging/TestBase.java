package com.kadroom.staging;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class TestBase implements TestData{
    public WebDriver driver;
    public WebDriverWait wait;

    public String typeOfCertificate;

    @Before
    public void start () {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);
    }

    @After
    public void stop () {
        driver.quit();
    }

    public void openCertificatePage () {
        driver.navigate().to(certificateURL);
    }
    public void openCertificatePageEn () {
        driver.navigate().to(certificateURLEn);
    }
    public void openCertificatePageUa () {
        driver.navigate().to(certificateURLUa);
    }

    public void selectCertificate (String type) {
        List<WebElement> certificateType = driver.findElements(By.xpath("//div[@class='cert__item-box contact-form js-give-certificate']"));
        for (WebElement webElement : certificateType) {
            typeOfCertificate = webElement.getAttribute("data-type");
            if (typeOfCertificate.equals(type)){

                if (typeOfCertificate.equals(all_inclusive)) {
                    webElement.click();
                } else if (typeOfCertificate.equals(quest_room)){
                    webElement.click();
                    Select select = new Select(rooms);
                    List options = select.getOptions();
                    int index = (int) (Math.random() * options.size());
                    select.selectByIndex(index);
                    do {
                        index = (int) (Math.random() * options.size());
                        select.selectByIndex(index);
                    } while (index == 1);
                }else if (typeOfCertificate.equals(cost)){
                    webElement.click();
                    Select select = new Select(prices);
                    List options = select.getOptions();
                    int index = (int) (Math.random() * options.size());
                    select.selectByIndex(index);
                    do {
                        index = (int) (Math.random() * options.size());
                        select.selectByIndex(index);
                    } while (index == 1);
                }
            }
        }
    }

    public void fillTheForm () {
        userName.sendKeys(name);
        userTel.sendKeys(tel);
        userEmail.sendKeys(email);
        userText.sendKeys(text);
        submitButtonForm.click();
    }

    public void incorrectFillTheForm () {
        userName.sendKeys(name);
        userTel.sendKeys(name);
        userEmail.sendKeys(email);
        userText.sendKeys(text);
        submitButtonForm.click();
        wait.until(ExpectedConditions.visibilityOf(incorrectNumber));
    }
    public void emptyFillTheForm () {
        submitButtonForm.click();
        wait.until(ExpectedConditions.visibilityOf(emptyForm));
    }

    public void incorrectNumberTrue () {
        Assert.assertEquals("Некорректный номер телефона.", incorrectNumber.getText());
    }
    public void emptyFormTrue () {
        Assert.assertEquals("Пожалуйста, укажите телефон или email!", emptyForm.getText());
    }

    @FindBy(name = "text-104")
    public WebElement userName;

    @FindBy(name = "tel-193")
    public WebElement userTel;

    @FindBy(name = "email-795")
    public WebElement userEmail;

    @FindBy(name = "textarea-726")
    public WebElement userText;

    @FindBy(xpath = "//input[@type='submit']")
    public WebElement submitButtonForm;

    @FindBy(id = "cf7-rooms")
    public WebElement rooms;

    @FindBy(id = "cf7-prices")
    public WebElement prices;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement incorrectNumber;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement emptyForm;
}
