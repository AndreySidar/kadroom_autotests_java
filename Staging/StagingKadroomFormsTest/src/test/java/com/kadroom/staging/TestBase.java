package com.kadroom.staging;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestBase implements TestData {

    public WebDriver driver;
    public WebDriverWait wait;

    @Before
    public void start () {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);
    }

    @After
    public void stop () {
        driver.quit();
    }

    public void openMainPage () {
        driver.navigate().to(mainUrl);
    }
    public void openMainPageEn () {
        driver.navigate().to(mainUrlEn);
    }
    public void openMainPageUa () {
        driver.navigate().to(mainUrlUa);
    }


    public void openContactPage () {
       driver.navigate().to(contactURL);
    }
    public void openContactPageEn () {
        driver.navigate().to(contactURLEn);
    }
    public void openContactPageUa () {
        driver.navigate().to(contactURLUa);
    }


    public void openCorporatePage () {
        driver.navigate().to(corporateURL);
    }


    public void openFranchisePage () {
        driver.navigate().to(franchiseURL);
    }
    public void openFranchisePageEn () {
        driver.navigate().to(franchiseURLEn);
    }
    public void openFranchisePageUa () {
        driver.navigate().to(franchiseURLUa);
    }

    public void writeUsLableClick () {
        writeUs.click();
    }

    public void buttonClick () {
        button.click();
    }

    public void fillTheForm () {
        userName.sendKeys(name);
        userTel.sendKeys(tel);
        userEmail.sendKeys(email);
        userText.sendKeys(text);
        submitButtonForm.click();
    }

    public void incorrectFillTheForm () {
        userName.sendKeys(name);
        userTel.sendKeys(name);
        userEmail.sendKeys(email);
        userText.sendKeys(text);
        submitButtonForm.click();
        wait.until(ExpectedConditions.visibilityOf(incorrectNumber));
    }

    public void emptyFillTheForm () {
        submitButtonForm.click();
        wait.until(ExpectedConditions.visibilityOf(emptyForm));
    }

    public void incorrectNumberTrue () {
        Assert.assertTrue("Некорректный номер телефона.", incorrectNumber.isDisplayed());
    }

    public void emptyFormTrue () {
        Assert.assertTrue("Пожалуйста, укажите телефон или email!", incorrectNumber.isDisplayed());
    }

    @FindBy(xpath = "//span[@class='writeus-title']")
    public WebElement writeUs;

    @FindBy(name = "text-104")
    public WebElement userName;

    @FindBy(name = "tel-193")
    public WebElement userTel;

    @FindBy(name = "email-795")
    public WebElement userEmail;

    @FindBy(name = "textarea-726")
    public WebElement userText;

    @FindBy(xpath = "//input[@type='submit']")
    public WebElement submitButtonForm;

    @FindBy(xpath = "//button")
    public WebElement button;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement incorrectNumber;

    @FindBy(xpath = "//span[@role='alert']")
    public WebElement emptyForm;
}
