package com.kadroom.staging;

public class MainTest extends TestSteps{
    @org.junit.Test
    public void testForms () {
        TestMainPage();
        TestContactPage();
        TestCorporatePage();
        TestFranchisePage();

        IncorrectNumberTest();
        EmptyFormPage();
    }
}
