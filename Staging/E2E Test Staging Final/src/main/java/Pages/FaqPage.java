package Pages;

import Entites.FormTest;
import TestData.TestData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class FaqPage implements TestData {

    private WebDriver driver;

    public FaqPage(WebDriver driver) {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
        this.driver = driver;
    }

    public void openFaqPage() {
        driver.navigate().to(faqURL);
    }

    public void openFaqPageEn() {
        driver.navigate().to(faqURLEn);
    }

    public void openFaqPageUa() {
        driver.navigate().to(faqURLUa);
    }

    public void fillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.clickWriteUsLable();
        formTest.fillTheForm();
    }

    public void incorrectFillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.clickWriteUsLable();
        formTest.incorrectFillTheForm();
        formTest.incorrectNumberTrue();
    }

    public void emptyFillForm() {

        FormTest formTest = new FormTest(driver);

        formTest.clickWriteUsLable();
        formTest.emptyFillTheForm();
        formTest.emptyFormTrue();
    }
}
